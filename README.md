LEAP Web Pages
==================================

This is the repository for the LEAP website at https://leap.se.

It is entirely static, but relies on a bunch of apache tricks for things like
language negotiation. A static website generator called `amber` is used to
render the source files into html files.

To submit changes, please fork this repo and issue pull requests on [github](https://github.com/leapcode/leap_se)
or [LEAP's gitlab instance](https://0xacab.org/leap/leap_se).
Menu changes are in the Amber subdirectory, Content and color changes are in
Pages.

For more information how to use `amber`, see:
https://github.com/leapcode/amber.

The 0xacab CI is setup to deploy automatically to the production website, see the
[0xacab CI environments](https://0xacab.org/leap/leap_se/environments/15). The steps
how this is done are configured in the [.gitlab-ci.yml](https://0xacab.org/leap/leap_se/blob/master/.gitlab-ci.yml) file.

To deploy the current version manually:

    sudo gem install capistrano
    git clone https://leap.se/git/leap_se
    cd leap_se
    cap deploy

You will need pubkey access to ssh://website@hare.leap.se for this to work.

The deploy pulls directly from master branch of https://leap.se/git/leap_se
(in other words, local changes are not deployed).
