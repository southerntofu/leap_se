@title = 'LEAP member received the 2018 Pizzigati Prize'
@author = 'isydor'
@posted_at = '2018-05-03'
@more = false
@preview_image = '/img/pages/tides_logo.png'

We are proud that [Tides](https://www.tides.org/) awarded LEAP
member Andre Bianchi the [2018 Pizzigati
Prize](https://www.tides.org/accelerating-social-change/2018-pizzigati-prize-andre-bianchi/). Thank you a lot for this recognition of the work done to make it
possible to freely communicate digitally and to support social change!
