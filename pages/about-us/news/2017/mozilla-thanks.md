@title = 'Big thanks to the Mozilla Foundation for supporting encrypted email!'
@author = 'micah'
@posted_at = '2017-04-10'
@more = false
@preview_image = '/img/pages/mozz/logo.png'

We are very excited to have been awarded the MOSS grant [to improve our encryption and syncing library](https://0xacab.org/leap/soledad/wikis/2017-roadmap), [Soledad](https://leap.se/soledad), the heart of our encrypted email system. This will help us enhance [Soledad](https://leap.se/soledad) to get us needed improvements in efficiency in syncing and encryption.
