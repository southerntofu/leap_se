@title = 'New UI for Bitmask Android VPN Coming soon!'
@author = 'cyberta'
@posted_at = '2017-11-27'
@more = false
@preview_image = '/img/pages/new_android_ui.png'

Have opinions about Android UI? We are updating the UI/UX and will release a new version in 2017. [Chime in on our issue tracker](https://0xacab.org/leap/bitmask_android/issues/8781#note_129570)

The Bitmask team.
